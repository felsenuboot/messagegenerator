//Object that contains subject + verb + object

//Message --> Question or Statement
const selectRandomEntryfromArray = array => array[Math.floor(Math.random() * array.length)];
const sentence = {
    subject: ['The House', 'The City', 'A Man in lego city', 'I', 'You', 'He', 'She', 'It', 'We', 'You', 'They'],
    verb: {
        linking: {
            am: ['am', 'shine', 'dream to be'],
            are: ['are', 'shine'],
            is: ['is', 'shines']
        },
        action: {
            transitive: {
                am: ['am raising'],
                are: ['are raising'],
                is: ['raises']
            }
        }
    },
    object: {
        nouns: ['a melon'],
        adjective: {
            descriptive: ['attractive', 'bald', 'beautiful', 'chubby','clean','dazzling','drab','elegant','fancy','fit','flabby','glamorous','gorgeous','handsome','long','magnificent','muscular','plain','plump','quaint','scruffy','shapely','short','skinny','stocky','ugly','unkempt','unsightly', 'ashy','black','blue','gray','green','icy','lemon','mango','orange','purple','red','salmon','white','yellow'],
        }
    },
    generate() {
        let subject = this.randomSubject();
        console.log("The subject is: " + subject);

        let conjugation = this.determineConjugation(subject);
        console.log("Hence the conjugation must be: " + conjugation + "\n");

        let verb = this.randomVerb(conjugation);
        
        let object = this.randomObject(verb);

        return subject + " " + verb.verb + " " + object + ".";
    },   
    randomSubject() {
        return selectRandomEntryfromArray(this.subject);
    },
    randomVerb(conjugation) {
        let verb = {};
        switch (Math.floor(Math.random() * 2)) {
            case 0:
                verb.verb = selectRandomEntryfromArray(this.verb.linking[conjugation]);
                verb.type = 'linking';
                break;
            case 1:
                verb.verb = selectRandomEntryfromArray(this.verb.action.transitive[conjugation]);
                verb.type = 'transitive';
                break;
            }
        return verb;
    },
    determineConjugation(subject) {
        switch (subject) {
            case 'I':
                conjugation = 'am';
                break;
            case 'You':
            case 'We':
            case 'They':
                conjugation = 'are';
                break;
            default:
                conjugation = 'is';
        }
        return conjugation;
    },
    randomObject(verb) {
        switch (verb.type) {
            case 'linking':
                return selectRandomEntryfromArray(this.object.adjective.descriptive);
            case 'transitive':
                return selectRandomEntryfromArray(this.object.nouns)
        }
        return selectRandomEntryfromArray(this.adjective.descriptive);
    }
}

for (let i = 0; i < 1; i++) {

    console.log(sentence.generate());
}
