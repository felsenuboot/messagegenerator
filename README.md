# Random Message Generator
An App, built in JS, returning a randomized message.

## Table of contents
* [General info](#general-info)
* [Screenshots](#screenshots)
* [Technologies](#technologies)
* [Setup](#setup)
* [Features](#features)
* [Status](#status)
* [Inspiration](#inspiration)
* [Contact](#contact)

## General info
I am trying to go even further an generate an completely randomized sentence.

    This project is meant to make myself more familiar with Git and especially practice Javascript.
    For generating sentences there are basic rules we must follow:

### Different Sentences

To generate sentences I have started to construct an object `sentence` with the needed properties. Its far from completion. Generally speking each sentence can be made up out of:
```mermaid
graph LR
    subgraph Sentences with adjectives
        Subject -.-> be
        be -.-> adjective
    end
```
But depending on the subject the verb changes and depending on the verb different object make or don't make sense, thats why this object starts to become so big.

```mermaid
graph TD
    subgraph Sentence
        subgraph Subject
            subjects[Array with subjects]
        end
        subgraph Verb
            linking[Linking verbs]
            subgraph Action verbs
                transitive[Transitive Verbs]
            end
        end
        subgraph Object
            nouns[Nouns]
            subgraph Adjectives
                descriptive[Descriptive Adj]
            end
        end
    end
```

Additionally to the semantic structuring of the elements of the sentence the object `sentence` has a few methods, determining the logic I mentioned above:

```mermaid
classDiagram
    class Sentence{
        String[] subject
        Object[] verb
        Object[] object

        generate()
        randomSubject()
        randomVerb(conjugation)
        determineConjugation(subject)
        randomObject(verb)
    }
```

## Technologies
* [js](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
* [mermaid](https://mermaid-js.github.io) _in case you are wondering how I did the graphs_

## Setup
`node messageGenerator.js`


## Features
List of features ready and TODOs for future development
* Simple Sentences with subject -> be -> adjective structure

To-do list:
* Add more verbs in `sentence.verb.linking[]` and `sentence.verb.action.transitive[]`
* Add More Complex Sentences

## Status
Project is: _in progress_ <!--, _finished_, _no longer continue_ and why?-->
Might continue on this in the future.


## Inspiration
[Codecademy](https://www.codecademy.com)
[Types of Verbs](https://www.uvu.edu/writingcenter/docs/handouts/grammar/typesofverbs.pdf)

## Contact
Created by [@felsenuboot](https://gitlab.com/felsenuboot) - feel free to contact me!

Created on 15th November 2020